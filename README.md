# Data-Cleaning (In this exercise i cleaned Nashville Housing Data i got from the web in MySQL.)

Welcome to my data analysis portfolio project! In this project,
I focused on data cleaning and preparation using the Nashville Housing dataset obtained from the web. 
The data required significant cleaning and transformation to be usable in MySQL,
and I utilized various data cleaning techniques such as handling missing values, removing duplicates, and dealing with outliers.
I also implemented data normalization techniques to optimize the database structure and improve its efficiency.
The project report provides detailed documentation on the cleaning and transformation process and how I utilized MySQL to achieve my objectives.
I hope you find this project informative and insightful!
